Prepare and Submit for Topic 1 a response for the following questions based on Chapter ONE material:  In teams of two students... 

1. Describe (in your words) what an SDLC is and explain each of the CORE processes (phases within).

"System Development Life Cycle", a process of building/developing a piece of software. Creating a detailed plan of how to develop, maintain, replace, and alter or enhance it.

The phases of this process are:
	- Planning
	- Defining
	- Designing
	- Building
	- Testing
	- Deployment


2. Compare and contrast the differences between a structured (prescribed) SDLC versus a more adaptive SDLC. Give an example of each.

The the two separate styles offer advantages and disadvantages to each other.

Structured relies on each step of the process being encapsulated and fully fleshed out before moving onto the next step of the process. This has the benefit of being very structured, once development begins, everyone has a very clear cut idea of what the product is as well as the goals/challenges they should face.

Unfortunately, because of the rigidity of this type of SDLC, developers are unable to diviate from what was laid out in the previous steps. Meaning that this philosophy doesn't allow for any encountered problems or optimizations that are discovered, and are instead seen as a lack of due-diligicence from the people completing the previous steps.

On the other hand, an Adaptive SDLC allows for a more flexible development process. Allowing for shifts in design or implementation, as long as they've been vetted by stakeholders. This allows for any sort of optimizations or issues discovered that may not have been entirely visible/recognizable in the original planning and designing process.

The drawback about this, is that often less time is spent in the initial development phases in an effort to begin development faster. This can sometimes result in the Plan and Design of the project being far more hazy early on and not having a great enough picture of what challenges or scope the project will have.


3. What is Agile development?
Agile is a more adaptive SDLC, it follows the typical development life-style phases mentioned above (Planning, Defining, Designing, Building, Testing, and Deployment), but it incorporates Stakeholder Review and Feedback sessions into the process.

The idea is that the software does not deploy without Stakeholder approval. So, after the feedback process, design is adjusted to incorporate feedback, the project time-table is adjusted, then the feedback is implemented into the software and the review, feedback process begins again.

This process will continue until the product receives stakeholder approval.
